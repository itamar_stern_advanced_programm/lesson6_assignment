﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class login : Form
    {
        public login()
        {
            InitializeComponent();

            StreamWriter writerN = new StreamWriter("name.txt");
            writerN.WriteLine("invalid");
            writerN.Close();
        }
        
        private void button1_Click(object sender, EventArgs e)
        {

			//open the file
            StreamReader reader = new StreamReader("users.txt");
            string line;
            bool good = false;

            while ((line = reader.ReadLine()) != null && !good)
            {

				//check if the username and password match
                if (line.Substring(0, line.IndexOf(',')).Equals(textBox1.Text) && line.Substring(line.IndexOf(',') + 1).Equals(textBox2.Text)) good = true;
            }

			//close the file
            reader.Close();

            if (good)
            {
                
                StreamWriter writerN = new StreamWriter("name.txt");
                writerN.WriteLine(textBox1.Text + "BD.txt");
                writerN.Close();

                this.Close();
            }
            else
            {
                textBox3.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

			//cancel button
            this.Close();
        }
    }
}
