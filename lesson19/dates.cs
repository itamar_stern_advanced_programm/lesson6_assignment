﻿using System;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class dates : Form
    {
        public dates()
        {
            
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
			//get the file with name that is the first line in the file "name.txt"
            StreamWriter writer = File.AppendText(new System.IO.StreamReader("name.txt").ReadLine());
			//add the event
            writer.WriteLine(textBox1.Text + "," + textBox3.Text + "," + monthCalendar1.SelectionRange.Start.ToShortDateString());
            writer.Close(); //close the file
            textBox1.Text = ""; //reset the textBox1 text field
            textBox3.Text = "celebrating his birthday"; //reset the textBox3 text field
            monthCalendar1.SetDate(monthCalendar1.TodayDate); //reset the month calender date
        }

        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {

            listBox1.Items.Clear(); //clear all of the previous items

            StreamReader reader = new StreamReader(new System.IO.StreamReader("name.txt").ReadLine());
            string line;

            while ((line = reader.ReadLine()) != null)
            {

				//check if the dates match
                if (line.Substring(line.LastIndexOf(',') + 1).Equals(monthCalendar1.SelectionRange.Start.ToShortDateString()))
                {

                    //add to the list box items
                    listBox1.Items.Add(line.Substring(0, line.IndexOf(',')) + " is " + line.Substring(line.IndexOf(',') + 1));
                }
            }
            
			//close the file
            reader.Close();

			//set the date picker time to the current selected date
            dateTimePicker1.Text = monthCalendar1.SelectionRange.Start.ToShortDateString();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            
			//update the month calender date
            monthCalendar1.SetDate(dateTimePicker1.Value);
        }
    }
}
